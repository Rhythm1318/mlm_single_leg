<?php

include("inc/connect.php");
include("inc/chkAuth.php");


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Update User's KYC Details</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php"><?php 
include("inc/company_name.php");
?></a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i></a>
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">You have 4 new notifications.</li>
            <div class="app-notification__content">
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Lisa sent you a mail</p>
                    <p class="app-notification__meta">2 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Mail server not working</p>
                    <p class="app-notification__meta">5 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Transaction complete</p>
                    <p class="app-notification__meta">2 days ago</p>
                  </div></a></li>
              <div class="app-notification__content">
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Lisa sent you a mail</p>
                      <p class="app-notification__meta">2 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Mail server not working</p>
                      <p class="app-notification__meta">5 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Transaction complete</p>
                      <p class="app-notification__meta">2 days ago</p>
                    </div></a></li>
              </div>
            </div>
            <li class="app-notification__footer"><a href="#">See all notifications.</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
	<?php
	include("inc/menu.php");
	?>
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> KYC Details</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">KYC Details</li>
        </ul>
      </div>
      <div class="row">
       <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
			
     
  
  <form class="form-inline" name="idpf" id="idpf" enctype="multipart/form-data" method="post" action="upload_idpf.php">
				
<!--<h2> ID-Proof</h2><br>-->

<div class="form-group mb-2">
                  <label class="control-label col-md-3"></label>
                  <div class="col-md-8">
                    <select class="form-control" name="idpf_doc_id" id="idpf_doc_id">

            <option value="">-- Select ID-Proof --</option>

<?php 

$sql = mysqli_query($conn, "SELECT * FROM list_doc");

while ($row = $sql->fetch_assoc()){

?>
<option value=<?php echo $row['doc_id']; ?>><?php echo $row['doc_name']; ?></option>

                 <?php
                     }
           ?>
 <option name="idpf_doc_id"> </option>   
    </select> 

            </div>

              </div>


 <div class="form-group mb-2">
    <label for="idpf_doc_no" class="control-label col-md-3"></label>
    <div class="col-md-8">
    <input name="idpf_doc_no" type="text" placeholder="Document Number" class="form-control" id="idpf_doc_no" required>
    </div>
  </div>


      <div class="form-group mb-2">

      Upload ID Proof:   <input type="file" name="fileToUpload" id="fileToUpload">
    </div>

 

  <input type="submit" name="submit_idpf" id="submit_idpf" value="Upload" class="btn btn-primary">



         </form>
<hr>


  <form class="form-inline" name="addrpf" id="addrpf" enctype="multipart/form-data" method="post" action="upload_addrpf.php">

  
<!--<h2> Address Proof</h2><br><br>-->




  <div class="form-group mb-2">
                  <label class="control-label col-md-3"></label>
                  <div class="col-md-8">
                    <select class="form-control" name="addr_doc_id" id="addr_doc_id">
            <option value="">-- Select Address-Proof --</option>
           <?php 

$sql = mysqli_query($conn, "SELECT * FROM list_doc");

while ($row = $sql->fetch_assoc()){

?>
<option value=<?php echo $row['doc_id']; ?>><?php echo $row['doc_name']; ?></option>

                 <?php
                     }
           ?>
 <option name="addr_doc_id"> </option>   
    </select> 

  </div>

</div>

<div class="form-group mb-2">
    <label for="addr_doc_no" class="control-label col-md-3"></label>
    <div class="col-md-8">
    <input name="addr_doc_no" type="text" placeholder="Document Number" class="form-control" id="addr_doc_no" required>
    </div>
  </div>

      Upload Address Proof:   <input type="file" name="fileToUpload" id="fileToUpload">

  

   <input type="submit" name="submit_addrpf" id="submit_addrpf" value="Upload" class="btn btn-primary">

        
        
        
              </form>
<hr>


      <form class="form-inline" name="pan" id="pan" enctype="multipart/form-data" method="post" action="upload_pan.php">


<!--<h2> PAN Card</h2><br>-->

<div class="form-group mb-2">
    <label for="pan_no" class="control-label col-md-3"></label>
    <div class="col-md-8">
    <input name="pan_no" type="text" placeholder="PAN Number" class="form-control" id="pan_no" required>
    </div>
  </div>


  Upload PAN<input type="file" name="fileToUpload" id="fileToUpload">

  

 <input type="submit" name="submit_pan" id="submit_pan" value="Upload" class="btn btn-primary">

        
        
        
              </form>

<?php





$sql="select * from kyc_detail where user_id=".$_SESSION['user_id'];

$rs=mysqli_query($conn,$sql);

echo "<br>";

?>
<div class="table-responsive-sm">
  <table class="table">
    <thead>
      <tr>
        <th>I-D Proof</th>
        <th>Address Proof</th>
        <th>PAN Card</th>
        

      </tr>
    </thead>
    <tbody>


<?php

$i=1;
while($row=mysqli_fetch_array($rs))

{
  echo "<tr>";

  if($row['idpf_doc_img']!="")
      echo "<td>"."<img src='uploads/kycdoc/".$row['idpf_doc_img']."' width=150 height=150>"."</td>";
  else
    echo "<td>NA</td>";

  if($row['addr_doc_img']!="")
      echo "<td>"."<img src='uploads/kycdoc/".$row['addr_doc_img']."' width=150 height=150>"."</td>";
  else
    echo "<td>NA</td>";


  if($row['pan_img']!="")
      echo "<td>"."<img src='uploads/kycdoc/".$row['pan_img']."' width=150 height=150>"."</td>";
  else
    echo "<td>NA</td>";
  

$i=$i+1;
  echo "</tr>";
  
}
  ?>
</tbody>
</table>

 
         
			
				
				
				
            </div>
            
            </div>
            
          </div>
        </div>
        <div class="clearix"></div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      
    </script>
  </body>
</html>