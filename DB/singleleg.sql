-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2020 at 10:33 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `singleleg`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `number` int(11) DEFAULT NULL,
  `admin_type` tinyint(4) DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` datetime DEFAULT NULL,
  `login_ip` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `name`, `number`, `admin_type`, `email`, `password`, `last_activity`, `login_ip`, `status`) VALUES
(1, 'rhythm', 1234567890, 1, 'rhythm@admin.com', '1234', '2020-06-05 13:06:39', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank_detail`
--

CREATE TABLE `bank_detail` (
  `b_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `acc_holdername` varchar(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `acc_no` int(11) DEFAULT NULL,
  `ifsc_code` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_addr` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cheque_img` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bank_detail`
--

INSERT INTO `bank_detail` (`b_id`, `user_id`, `bank_name`, `acc_holdername`, `acc_no`, `ifsc_code`, `branch_addr`, `cheque_img`, `update_date`, `status`) VALUES
(8, 1, 'kotak', 'Rhythm', 654332, 'www2', 'cv', '1-', '2020-05-26', 2),
(19, 42, 'kotak', 'Raj', 66778845, 'rr44', 'Punjabi bagh', '42-WhatsApp Image 2020-05-09 at 2.41.27 AM.jpeg', '2020-05-26', 2),
(20, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(21, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(22, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(23, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(24, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(25, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(26, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(27, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(28, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(29, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(30, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `daily_income`
--

CREATE TABLE `daily_income` (
  `inc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inc_lvl` int(11) NOT NULL,
  `inc_amt` double NOT NULL,
  `inc_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `daily_income`
--

INSERT INTO `daily_income` (`inc_id`, `user_id`, `inc_lvl`, `inc_amt`, `inc_date`, `status`) VALUES
(70, 1, 3, 500, '2020-06-02', 2),
(71, 1, 3, 500, '2020-06-05', 2),
(72, 1, 2, 200, '2020-06-05', 2),
(73, 1, 1, 100, '2020-06-05', 2),
(74, 43, 1, 50, '2020-06-05', 2);

-- --------------------------------------------------------

--
-- Table structure for table `gen_income`
--

CREATE TABLE `gen_income` (
  `inc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inc_for` int(11) NOT NULL,
  `inc_lvl` int(11) NOT NULL,
  `inc_amt` double NOT NULL,
  `inc_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gen_income`
--

INSERT INTO `gen_income` (`inc_id`, `user_id`, `inc_for`, `inc_lvl`, `inc_amt`, `inc_date`, `status`) VALUES
(64, 43, 45, 1, 100, '2020-05-31', 2),
(65, 1, 45, 2, 10, '2020-05-31', 2),
(66, 1, 44, 1, 100, '2020-06-03', 0),
(67, 1, 44, 1, 100, '2020-06-03', 0),
(68, 1, 42, 1, 100, '2020-06-03', 0),
(69, 1, 43, 1, 100, '2020-06-03', 0),
(70, 1, 43, 1, 100, '2020-06-03', 0),
(71, 1, 46, 1, 100, '2020-06-03', 0),
(72, 1, 50, 1, 100, '2020-06-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gen_income_setting`
--

CREATE TABLE `gen_income_setting` (
  `gen_id` int(11) NOT NULL,
  `lvl_number` int(11) NOT NULL,
  `lvl_income` int(11) NOT NULL,
  `update_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gen_income_setting`
--

INSERT INTO `gen_income_setting` (`gen_id`, `lvl_number`, `lvl_income`, `update_date`, `status`) VALUES
(1, 1, 100, '2020-05-31', 1),
(2, 2, 10, '2020-05-31', 1),
(3, 3, 10, '2020-05-31', 1),
(4, 4, 10, '2020-05-31', 1),
(5, 5, 10, '2020-05-31', 1),
(6, 6, 10, '2020-05-31', 1),
(7, 7, 10, '2020-05-31', 1),
(8, 8, 10, '2020-05-31', 1),
(9, 9, 10, '2020-05-31', 1),
(10, 10, 10, '2020-05-31', 1),
(11, 11, 10, '2020-06-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kyc_detail`
--

CREATE TABLE `kyc_detail` (
  `kyc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `idpf_doc_id` int(11) DEFAULT NULL,
  `idpf_doc_no` int(11) DEFAULT NULL,
  `idpf_doc_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `addr_doc_id` int(11) DEFAULT NULL,
  `addr_doc_no` int(11) DEFAULT NULL,
  `addr_doc_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pan_no` int(11) DEFAULT NULL,
  `pan_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `kyc_status` tinyint(4) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kyc_detail`
--

INSERT INTO `kyc_detail` (`kyc_id`, `user_id`, `idpf_doc_id`, `idpf_doc_no`, `idpf_doc_img`, `addr_doc_id`, `addr_doc_no`, `addr_doc_img`, `pan_no`, `pan_img`, `update_date`, `kyc_status`) VALUES
(10, 1, 4, 2233344, '31-ID-book.png', 5, 111111, '31-ADDR-Screenshot_2020-05-01-12-32-25-88.jpg', 22222, '31-PAN-3ddc60e2-bd74-4c8f-8ff9-718691d73dbb.jpg', '2020-05-26', 1),
(14, 42, 1, 99889987, '42-ID-d.jpg', 4, 45675, '42-ADDR-rr.jpg', 6667, '42-PAN-3a56461b-597a-4187-a8a6-2aa858429059.jpg', '2020-05-26', 2),
(15, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(16, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(17, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(18, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(19, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(20, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(21, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(22, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(23, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(24, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(25, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `list_doc`
--

CREATE TABLE `list_doc` (
  `doc_id` int(11) NOT NULL,
  `doc_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `list_doc`
--

INSERT INTO `list_doc` (`doc_id`, `doc_name`, `status`) VALUES
(1, 'aadhar card', 1),
(2, 'voter-id', 1),
(3, 'ration card', 1),
(4, 'license', 1),
(5, 'passport', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payout`
--

CREATE TABLE `payout` (
  `pid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `closing_date` date DEFAULT NULL,
  `tds` double DEFAULT NULL,
  `admin_charge` double DEFAULT NULL,
  `net_amt` double NOT NULL,
  `pay_method` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `payout_date` date DEFAULT NULL,
  `prev_bal` double NOT NULL DEFAULT '0',
  `cur_bal` double NOT NULL DEFAULT '0',
  `pay_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1-credit 2-debit',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payout`
--

INSERT INTO `payout` (`pid`, `user_id`, `amount`, `closing_date`, `tds`, `admin_charge`, `net_amt`, `pay_method`, `pay_detail`, `payout_date`, `prev_bal`, `cur_bal`, `pay_type`, `pay_status`) VALUES
(21, 1, 800, '2020-06-05', 5, 10, 680, 'cash', 'cash', '2020-06-05', 0, 680, 1, 1),
(22, 43, 50, '2020-06-05', 20, 10, 35, 'cash', 'cash', '2020-06-05', 0, 35, 1, 1),
(23, 1, 0, NULL, NULL, NULL, 680, NULL, NULL, '2020-06-05', 680, 0, 2, 1),
(24, 43, 0, NULL, NULL, NULL, 35, NULL, NULL, '2020-06-05', 35, 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pins`
--

CREATE TABLE `pins` (
  `pin_id` int(11) NOT NULL,
  `pin_number` double NOT NULL,
  `gen_date` date NOT NULL,
  `pin_amt` double NOT NULL DEFAULT '1000',
  `assign_to` int(11) NOT NULL,
  `used_by` int(11) DEFAULT NULL,
  `used_date` date DEFAULT NULL,
  `pin_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pins`
--

INSERT INTO `pins` (`pin_id`, `pin_number`, `gen_date`, `pin_amt`, `assign_to`, `used_by`, `used_date`, `pin_status`) VALUES
(1, 371119595845, '2020-06-03', 1000, 1, 44, '2020-06-03', 1),
(2, 313864623882, '2020-06-03', 1000, 1, 42, '2020-06-03', 1),
(3, 963339554329, '2020-06-03', 1000, 1, 43, '2020-06-03', 1),
(4, 365641141596, '2020-06-03', 1000, 1, 50, '2020-06-03', 1),
(5, 134299350203, '2020-06-03', 1000, 1, NULL, NULL, 0),
(6, 310071952823, '2020-06-03', 1000, 1, NULL, NULL, 0),
(7, 297357494083, '2020-06-03', 1000, 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `joining_fee` double NOT NULL DEFAULT '1000',
  `min_payout` int(11) NOT NULL DEFAULT '500',
  `tds` double NOT NULL DEFAULT '5',
  `tds_no_pan` double NOT NULL DEFAULT '20',
  `admin_charge` double NOT NULL DEFAULT '10',
  `booster_days` int(4) NOT NULL DEFAULT '50',
  `booster_income` double NOT NULL DEFAULT '100',
  `dir_req_b` int(11) NOT NULL,
  `super_booster_income` double NOT NULL,
  `super_booster_days` int(11) NOT NULL,
  `dir_req_sb` int(11) NOT NULL,
  `booster_max_days` int(11) NOT NULL,
  `rank_income` tinyint(4) NOT NULL DEFAULT '1',
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `joining_fee`, `min_payout`, `tds`, `tds_no_pan`, `admin_charge`, `booster_days`, `booster_income`, `dir_req_b`, `super_booster_income`, `super_booster_days`, `dir_req_sb`, `booster_max_days`, `rank_income`, `update_date`) VALUES
(1, 1000, 500, 5, 20, 10, 50, 100, 0, 0, 0, 0, 0, 1, '2020-06-02 05:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `singleleg_income_setting`
--

CREATE TABLE `singleleg_income_setting` (
  `lvl_id` int(11) NOT NULL,
  `lvl_number` tinyint(4) NOT NULL,
  `member_count` int(11) NOT NULL DEFAULT '0',
  `lvl_income` int(11) NOT NULL DEFAULT '0',
  `dir_req` int(11) NOT NULL DEFAULT '0',
  `days` int(11) NOT NULL DEFAULT '100',
  `update_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `singleleg_income_setting`
--

INSERT INTO `singleleg_income_setting` (`lvl_id`, `lvl_number`, `member_count`, `lvl_income`, `dir_req`, `days`, `update_date`, `status`) VALUES
(2, 1, 1, 50, 1, 100, '2020-05-29', 1),
(3, 2, 5, 200, 2, 100, '2020-05-29', 1),
(4, 3, 10, 500, 3, 100, '2020-05-29', 1),
(5, 4, 15, 600, 4, 100, '2020-05-31', 1),
(6, 5, 20, 700, 5, 100, '2020-05-31', 1),
(7, 6, 20, 800, 6, 100, '2020-05-31', 1),
(8, 7, 30, 900, 7, 100, '2020-05-31', 1),
(9, 8, 35, 1000, 8, 100, '2020-05-31', 1),
(10, 9, 40, 1100, 9, 100, '2020-05-31', 1),
(11, 10, 50, 1500, 10, 100, '2020-05-31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sponsor_id` int(11) DEFAULT NULL,
  `spill_id` int(11) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mobile` double NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `join_date` date NOT NULL,
  `activation_date` date DEFAULT NULL,
  `booster` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0- no booster, 1- booster, 2-super booster',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0',
  `last_activity` datetime DEFAULT NULL,
  `login_ip` varchar(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `member_id`, `name`, `sponsor_id`, `spill_id`, `email`, `mobile`, `password`, `join_date`, `activation_date`, `booster`, `pay_status`, `last_activity`, `login_ip`, `status`) VALUES
(1, 0, 'Rhythm', 0, 0, 'rhythm.8@live.com', 3443342334, '1234', '2020-05-05', '2020-05-31', 1, 1, '2020-06-05 13:02:30', '::1', 1),
(42, 0, 'Raj', 1, 1, 'raj@gmail.com', 6543218976, '1234', '2020-05-26', '2020-06-03', 0, 1, '2020-05-26 17:08:28', '::1', 1),
(43, 0, 'Rajni', 1, 42, 'rajni@gmail.com', 4455334532, '1234', '2020-05-26', '2020-06-03', 0, 1, '2020-05-28 17:31:00', '::1', 1),
(44, 0, 'Riya', 1, 43, 'riya@gmail.com', 5434567865, '1234', '2020-05-26', '2020-06-03', 0, 1, NULL, NULL, 1),
(45, 0, 'sachin', 43, 44, 'sachin@gmail.com', 9999944444, '1234', '2020-05-26', '2020-05-31', 0, 1, '2020-05-29 12:23:12', '::1', 1),
(46, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', '2020-06-03', 0, 1, NULL, NULL, 1),
(47, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(48, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(49, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(50, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', '2020-06-03', 0, 1, NULL, NULL, 1),
(51, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(52, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(53, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(54, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(55, 0, 'Virat', 1, 45, 'v@gmail.com', 5544557766, '1234', '2020-05-31', NULL, 0, 1, NULL, NULL, 1),
(56, 0, 'Subhash', 1, 55, 'subhash@gmail.com', 88221144, '1234', '2020-06-04', NULL, 0, 0, NULL, NULL, 1),
(58, 0, 'sonu', 1, 56, 'sonu@gmail.com', 11100011, '1234', '2020-06-04', NULL, 0, 0, NULL, NULL, 1),
(59, 0, 'sonus', 1, 58, 'sonus@gmail.com', 211100011, '1234', '2020-06-04', NULL, 0, 0, NULL, NULL, 1),
(60, 0, 'warner', 1, 59, 'warner@gmail.com', 9000211100011, '1234', '2020-06-04', NULL, 0, 0, NULL, NULL, 1),
(61, 0, 'ytrew', 1, 60, 'qwert', 555444, '1234', '2020-06-04', NULL, 0, 0, NULL, NULL, 1),
(62, 0, 'ram', 1, 61, 'ram333', 1094, '1234', '2020-06-04', NULL, 0, 0, NULL, NULL, 1),
(63, 139500, 'Sahil', 1, 62, 'sahil@gmail.com', 5550, '1234', '2020-06-04', NULL, 0, 0, '2020-06-04 16:23:03', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `acc_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `income_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `income_for` int(11) NOT NULL,
  `income_amt` double NOT NULL DEFAULT '0',
  `income_date` date NOT NULL,
  `prev_bal` double NOT NULL,
  `cur_bal` double NOT NULL DEFAULT '0',
  `update_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`acc_id`, `user_id`, `income_type`, `income_for`, `income_amt`, `income_date`, `prev_bal`, `cur_bal`, `update_date`, `status`) VALUES
(97, 43, 'Direct income', 45, 100, '2020-05-28', 0, 100, '2020-05-28', 1),
(98, 1, 'Direct income', 42, 100, '2020-05-28', 0, 100, '2020-05-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_payment`
--

CREATE TABLE `user_payment` (
  `pay_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pay_amt` double NOT NULL,
  `pay_method` tinyint(4) NOT NULL,
  `pay_detail` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pay_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_payment`
--

INSERT INTO `user_payment` (`pay_id`, `user_id`, `pay_amt`, `pay_method`, `pay_detail`, `pay_date`, `status`) VALUES
(57, 45, 1000, 1, 'Joining Fee', '2020-05-28', 1),
(58, 42, 1000, 2, 'joining fee', '2020-05-28', 1),
(59, 45, 1000, 2, 'joining fee', '2020-05-31', 1),
(62, 45, 1000, 1, 'ffff', '2020-05-31', 1),
(63, 45, 1000, 3, 'ggggg', '2020-05-31', 1),
(64, 45, 1000, 3, 'ggg', '2020-05-31', 1),
(66, 44, 1000, 4, 'PIN', '2020-06-03', 1),
(67, 42, 1000, 4, 'pin', '2020-06-03', 1),
(68, 42, 1000, 4, '313864623882', '2020-06-03', 1),
(69, 43, 1000, 4, '963339554329', '2020-06-03', 1),
(70, 43, 1000, 4, '963339554329', '2020-06-03', 1),
(71, 46, 1000, 1, 'cash', '2020-06-03', 1),
(72, 50, 1000, 4, 'pin', '2020-06-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `prof_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dob` date DEFAULT NULL,
  `city` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` double DEFAULT NULL,
  `company_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `office_addr` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profile`
--

INSERT INTO `user_profile` (`prof_id`, `user_id`, `dob`, `city`, `state`, `address`, `pincode`, `company_name`, `office_addr`, `job_type`, `update_date`, `status`) VALUES
(12, 1, '2020-05-11', 'delhi', 'new delhi', 'paschim vihar', 110063, 'AR International', 'Patel Nagar', 'Private', '2020-05-26', 1),
(36, 42, '2020-01-06', 'delhi', 'new delhi', 'mayur vihar', 110063, 'lifestyle', 'paschim vihar', 'Private', '2020-05-26', 1),
(37, 43, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(38, 44, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(39, 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(40, 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(41, 57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(42, 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(43, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(44, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(45, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(46, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(47, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `bank_detail`
--
ALTER TABLE `bank_detail`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `daily_income`
--
ALTER TABLE `daily_income`
  ADD PRIMARY KEY (`inc_id`);

--
-- Indexes for table `gen_income`
--
ALTER TABLE `gen_income`
  ADD PRIMARY KEY (`inc_id`);

--
-- Indexes for table `gen_income_setting`
--
ALTER TABLE `gen_income_setting`
  ADD PRIMARY KEY (`gen_id`);

--
-- Indexes for table `kyc_detail`
--
ALTER TABLE `kyc_detail`
  ADD PRIMARY KEY (`kyc_id`);

--
-- Indexes for table `list_doc`
--
ALTER TABLE `list_doc`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `payout`
--
ALTER TABLE `payout`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `pins`
--
ALTER TABLE `pins`
  ADD PRIMARY KEY (`pin_id`),
  ADD UNIQUE KEY `pin_number` (`pin_number`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `singleleg_income_setting`
--
ALTER TABLE `singleleg_income_setting`
  ADD PRIMARY KEY (`lvl_id`),
  ADD UNIQUE KEY `lvl_number` (`lvl_number`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `user_payment`
--
ALTER TABLE `user_payment`
  ADD PRIMARY KEY (`pay_id`);

--
-- Indexes for table `user_profile`
--
ALTER TABLE `user_profile`
  ADD PRIMARY KEY (`prof_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bank_detail`
--
ALTER TABLE `bank_detail`
  MODIFY `b_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `daily_income`
--
ALTER TABLE `daily_income`
  MODIFY `inc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `gen_income`
--
ALTER TABLE `gen_income`
  MODIFY `inc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `gen_income_setting`
--
ALTER TABLE `gen_income_setting`
  MODIFY `gen_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kyc_detail`
--
ALTER TABLE `kyc_detail`
  MODIFY `kyc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `list_doc`
--
ALTER TABLE `list_doc`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payout`
--
ALTER TABLE `payout`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pins`
--
ALTER TABLE `pins`
  MODIFY `pin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `singleleg_income_setting`
--
ALTER TABLE `singleleg_income_setting`
  MODIFY `lvl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `user_payment`
--
ALTER TABLE `user_payment`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `user_profile`
--
ALTER TABLE `user_profile`
  MODIFY `prof_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
