<?php 
include("inc/connect.php");
include("inc/userAuth.php");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Product- Edit </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">Ndspl</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
       
               <ul class="app-notification dropdown-menu dropdown-menu-right">
           
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
          
            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
	<?php
	include("inc/menu.php");
	?>
   <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> Product</h1>
          <p>Edit</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Product</li>
          <li class="breadcrumb-item"><a href="add_product.php">Add New</a></li>
        </ul>
      </div>
      <div class="row">
       <div class="col-md-12">
          <div class="tile">
            <h3 class="tile-title">Edit </h3>
            <div class="tile-body">
			<?php 
				if(isset($_POST['submit'])){
					//print_r($_POST);
					$p_id = $_POST['prodID'];
					$cat_id = $_POST['category'];
					$prodName = $_POST['prodName'];
					$partNo = $_POST['partNo'];
					$description = $_POST['description'];
					$prodQty = $_POST['prodQty'];
					$pur_price = $_POST['pur_price'];
					$msp = $_POST['msp'];
					$restock = $_POST['restock'];
					$min_vol = $_POST['min_vol'];
					
					
					$query = "Update products set cat_id='$cat_id',prod_name='$prodName',part_no='$partNo',prod_details='$description',prod_qty='$prodQty',purchase_price='$pur_price',dist_restock='$restock',dist_min_vol='$min_vol',update_date=NOW(),prod_status='1'  where prod_id='$p_id'";
					
					$update = mysqli_query($con,$query);
					
					
					if($update){
						
						//echo '<script>alert("Product updated successfully.")</script>';
						$url='product_mgmt.php?msg=+Product updated successfully';
						
						echo "<script>window.location.href='$url';</script>";
					}
					else{
						echo '<script>alert("sorry , There was problem.")</script>';
					}
				
				}	
				
				$p_id = $_GET['id'];
				$sql = "select * from products where prod_id='$p_id'";
				
				$res = mysqli_query($con,$sql);
				$rowItem = mysqli_fetch_assoc($res);
				//print_r($rowItem);
			?>
              <form class="form-horizontal" id="addItem" enctype="multipart/form-data" method="post" action="">
				<input type="hidden"  name="prodID" value="<?php echo $p_id ;?>">
				<div class="form-group row">
                  <label class="control-label col-md-3">Category</label>
                  <div class="col-md-8">
                    <select class="form-control" name="category" >
						<option value="">-select category-</option>
						<?php
						$rs = mysqli_query($con,"SELECT * FROM category");
						
						while ($row = mysqli_fetch_array($rs)) {
							$select ='';
							$cat_id = $row["cat_id"];
							$cat_name = $row["cat_name"];
							if($rowItem['cat_id']==$cat_id){
							?>
							<option value="<?php echo $cat_id; ?>" selected><?php echo $cat_name; ?></option>
							<?php
						   }
						   else{ ?>
							 <option value="<?php echo $cat_id; ?>"><?php echo $cat_name; ?></option>
							<?php							 
						   }
						}
						?>
					</select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3"> Product Name</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter product name" name="prodName" value="<?php echo $rowItem['prod_name'];?>" required>
                  </div>
                </div>
				
				
                
                <div class="form-group row">
                  <label class="control-label col-md-3"> Part Number</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter part no" name="partNo"  value="<?php echo $rowItem['part_no'];?>" required>
                  </div>
                </div>
				<div class="form-group row">
                  <label class="control-label col-md-3">Description</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" placeholder="Enter product description" name="description"><?php echo $rowItem['prod_details'];?></textarea>
                  </div>
                </div>
				<div class="form-group row">
                  <label class="control-label col-md-3"> Quantity</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter quantity" name="prodQty" value="<?php echo $rowItem['prod_qty'];?>" required>
                  </div>
                </div>
				<div class="form-group row">
                  <label class="control-label col-md-3"> Purchase Price</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter purchase price" name="pur_price" value="<?php echo $rowItem['purchase_price'];?>"required>
                  </div>
                </div>
				<div class="form-group row">
                  <label class="control-label col-md-3"> MSP</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter msp" name="msp" value="<?php echo $rowItem['prod_msp'];?>"required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3"> Dist Restock Lts</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter restock lt" name="restock" value="<?php echo $rowItem['dist_restock'];?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3"> Dis Pricing Min Vol</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter disty pricing min vol" name="min_vol" value="<?php echo $rowItem['dist_min_vol'];?>"required>
                  </div>
                </div>
                
				
				<div class="form-group row">
                  <div class="col-md-8 col-md-offset-9">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="submit" name="submit" value="Update" class="btn btn-primary">
                      </label>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            
          </div>
        </div>
        <div class="clearix"></div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
  </body>
</html>