<?php 
include("inc/connect.php");
include("inc/userAuth.php");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Food Item - Edit </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">Paprikas</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i></a>
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">You have 4 new notifications.</li>
            <div class="app-notification__content">
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Lisa sent you a mail</p>
                    <p class="app-notification__meta">2 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Mail server not working</p>
                    <p class="app-notification__meta">5 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Transaction complete</p>
                    <p class="app-notification__meta">2 days ago</p>
                  </div></a></li>
              <div class="app-notification__content">
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Lisa sent you a mail</p>
                      <p class="app-notification__meta">2 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Mail server not working</p>
                      <p class="app-notification__meta">5 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Transaction complete</p>
                      <p class="app-notification__meta">2 days ago</p>
                    </div></a></li>
              </div>
            </div>
            <li class="app-notification__footer"><a href="#">See all notifications.</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
	<?php
	include("inc/menu.php");
	?>
   <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> Food Item</h1>
          <p>Edit</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Food Item</li>
          <li class="breadcrumb-item"><a href="add_food_item.php">Add New</a></li>
        </ul>
      </div>
      <div class="row">
       <div class="col-md-12">
          <div class="tile">
            <h3 class="tile-title">Edit </h3>
            <div class="tile-body">
			<?php 
				if(isset($_POST['submit'])){
					//print_r($_POST);
					$item_id = $_POST['itemID'];
					$cat_id = $_POST['category'];
					$itemName = $_POST['itemName'];
					$description = $_POST['description'];
					$itemPrice = $_POST['itemPrice'];
					//$itemName = $_FILES['file'];
					//get old file name
					$sql = "select * from food_items where item_id='$item_id'";
					$resFile = mysqli_query($con,$sql);
					$rowFile = mysqli_fetch_assoc($resFile);
					$oldFileName = $rowFile['img_name'];
					$image_delete = "itemImages/".$oldFileName;
					///
					$errors= array();
					$file_name = time().$_FILES['file']['name'];
					$img_update='';
					if($file_name){
						$file_size = $_FILES['file']['size'];
						$file_tmp = $_FILES['file']['tmp_name'];
						//$file_type= $_FILES['file']['type'];
						$fileExt = explode('.',$file_name);
						$file_ext=strtolower($fileExt[1]);

						$extensions= array("jpeg","jpg","png","gif");

						if(in_array($file_ext,$extensions)=== false){
						 $errors[]="extension not allowed, please choose a JPEG or PNG file.";
						}

						if($file_size > 2097152){
						 $errors[]='File size must be excately 2 MB';
						}

						if(empty($errors)==true){
						 move_uploaded_file($file_tmp,"itemImages/".$file_name);
						 
						 $img_update = " , img_name='$file_name'";
						 
							if (unlink($image_delete)) { 
							                                   
							} else {
							                                          
							}
						}else{
						 //print_r($errors);
						 $img_update='';
						}
						
						
					}
					
					$query = "Update food_items set cat_id='$cat_id',item_name='$itemName',item_details='$description',item_price='$itemPrice',update_date=NOW(),item_status='1' ".$img_update." where item_id='$item_id'";
					$update = mysqli_query($con,$query);
					if($update){
						//header('Location:item_mgmt.php');
						echo '<script>window.location.href = "item_mgmt.php";</script>';
					}
					else{
						echo '<script>alert("sorry , There was problem.")</script>';
					}
				
				}	
				
				$item_id = $_GET['id'];
				$sql = "select * from food_items where item_id='$item_id'";
				
				$res = mysqli_query($con,$sql);
				$rowItem = mysqli_fetch_assoc($res);
				//print_r($rowItem);
			?>
              <form class="form-horizontal" id="addItem" enctype="multipart/form-data" method="post" action="">
				<input type="hidden"  name="itemID" value="<?php echo $item_id ;?>">
				<div class="form-group row">
                  <label class="control-label col-md-3">Category</label>
                  <div class="col-md-8">
                    <select class="form-control" name="category" >
						<option value="">-select category-</option>
						<?php
						$rs = mysqli_query($con,"SELECT * FROM category");
						
						while ($row = mysqli_fetch_array($rs)) {
							$select ='';
							$cat_id = $row["cat_id"];
							$cat_name = $row["cat_name"];
							if($rowItem['cat_id']==$cat_id){
							?>
							<option value="<?php echo $cat_id; ?>" selected><?php echo $cat_name; ?></option>
							<?php
						   }
						   else{ ?>
							 <option value="<?php echo $cat_id; ?>"><?php echo $cat_name; ?></option>
							<?php							 
						   }
						}
						?>
					</select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-3"> Item Name</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter item name" name="itemName" value="<?php echo $rowItem['item_name'];?>" required>
                  </div>
                </div>
				
				<div class="form-group row">
                  <label class="control-label col-md-3"> Item Price</label>
                  <div class="col-md-8">
                    <input class="form-control" type="text" placeholder="Enter item price" name="itemPrice" value="<?php echo $rowItem['item_price'];?>" required>
                  </div>
                </div>
                
                <div class="form-group row">
                  <label class="control-label col-md-3">Description</label>
                  <div class="col-md-8">
                    <textarea class="form-control" rows="4" placeholder="Enter item description" name="description"><?php echo $rowItem['item_details'];?></textarea>
                  </div>
                </div>
                
                <div class="form-group row">
                  <label class="control-label col-md-3">Item Image</label>
                  <div class="col-md-8">
                    <input class="form-control" type="file" name="file" >
                  </div>
					<div class="col-md-8" >
					  <img src="itemImages/<?php echo $rowItem['img_name']; ?>" alt="" width="200" height="200" style="float:right; margin-top:5px;">
					</div>
				</div>
                
				
				<div class="form-group row">
                  <div class="col-md-8 col-md-offset-9">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="submit" name="submit" value="Update" class="btn btn-primary">
                      </label>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            
          </div>
        </div>
        <div class="clearix"></div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
  </body>
</html>