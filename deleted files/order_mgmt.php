<?php 
include("inc/connect.php");
include("inc/userAuth.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Order Management - Ndspl Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">Ndspl</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        
       
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
           
            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <?php
	include("inc/menu.php");
	?>
      
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> Order Manegment</h1>
          <p>Order List</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Order Manegment</li>
          <li class="breadcrumb-item"><a href="add_order.php">Add Order</a></li>
        </ul>
      </div>
    
	<div class="row">
        <div class="col-md-12">
            <div class="tile">
				<div class="table-responsive">
				  <table class="table table-hover table-bordered" id="catTable">
					<thead>
					  <tr>
						<th>Order ID</th>
						<th>Product</th>
						<th>Category</th>
						<th>Part No</th>
						
						
						<th>Qty</th>
						<th>Order Price</th>
						<th>Vendor</th>
					
						<th>Order Date</th>
						
						<th>Action</th>
						
					  </tr>
					</thead>
						<tbody>
						<?php
							
								$sql = "SELECT o.*,p.prod_name,p.part_no, c.cat_name FROM `orders` o, products p, category c where p.prod_id=o.prod_id 
										and c.cat_id=p.cat_id and o.prod_id=p.prod_id order by o.ord_id desc";
								$rs = mysqli_query($con,$sql);
								
								while($row = mysqli_fetch_array($rs))
								{   $pname=$row['prod_name'];
							        
									
									
									echo "<tr><td>".$row['ord_id']."</td>";
									echo "<td>".$pname."</td>";
									echo "<td>".$row['cat_name']."</td>";
									echo "<td>".$row['part_no']."</td>";
									//echo "<td>".$row['prod_details']."</td>";
									echo "<td>".$row['ord_qty']."</td>";
									echo "<td>".$row['ord_price']."</td>";
									echo "<td>".$row['ord_vendor']."</td>";
									echo "<td>".$row['ord_date']."</td>";
									$str="<a href=del_order.php?id=".$row['ord_id']."&qty=".$row['ord_qty']."&pid=".$row['prod_id'].">Delete</a>";
									
                                    echo "<td>".$str."</td>" ;
									echo "</tr>";
								
									
								}
							
							
							
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
	<script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
	<script>
	
	 function addCat(cname,cid,act) {
		 //alert(cname);
                $.ajax({type: "POST",
                    url: "add_cat.php",
                    data: "cname=" + cname + "&action=" + act + "&cid=" +cid,
                    async: false,
                    success: function (response) {
						//alert(response);
                        var response_1 = response.charAt();
                        if (response_1 == "f") {
                            $("#error_text").html("Please try again");
                            $("#errorblock").show();
                            setTimeout('$("#errorblock").fadeOut("slow");', 2000);
                            return false;
                        }
						else if (response_1 == "d") {
                            $("#error_text").html("This category alreday Exists");
                            $("#errorblock").show();
                            setTimeout('$("#errorblock").fadeOut("slow");', 2000);
                            return false;
                        }
						else if (response_1 == "s") {
							//alert("success");
                            $("#errorblock").hide();
                            $("#category_name").val('');
                            $("#success_text").html("Saved successfully");
                            $("#successblock").show();
                            setTimeout('$("#successblock").fadeOut("slow");', 2000);
							
							setTimeout('window.location.reload();', 1000);
                            if(type !=""){
                                setTimeout('window.location.reload();', 2000);
                            }
                         }
                    }
                });
            }
			
			
			
			function delCat(cid) {
		     var ans=confirm("Are you sure, you want to delete this category?");
			 if(ans==true){
                $.ajax({type: "POST",
                    url: "add_cat.php",
                    data: "catID=" + cid + "&action=delete" ,
                    async: false,
                    success: function (response) {
						//alert(response);
                        var response_1 = response.charAt();
                        if (response_1 == "f") {
                            $("#error_text").html("Please try again");
                            $("#errorblock").show();
                            setTimeout('$("#errorblock").fadeOut("slow");', 2000);
                            return false;
                        }
						else if (response_1 == "s") {
							//alert("success");
                            $("#errorblock").hide();
                            $("#category_name").val('');
                            $("#success_text").html("Category deleted successfully");
                            $("#successblock").show();
                            setTimeout('$("#successblock").fadeOut("slow");', 2000);
							
							setTimeout('window.location.reload();', 1000);
                            if(type !=""){
                                setTimeout('window.location.reload();', 2000);
                            }
                         }
                    }
                });
            }
			}
			
			
			function editCat(cid) {
			   var str1=cid;
			   var arrCat = str1.split("-");
			   $("#catid").val(arrCat[0]);
			   $("#cat_name").val(arrCat[1]);
				}
			
	
	
$(document).ready( function () {  
$("#errorblock").hide();
$("#successblock").hide();
$('#catTable').DataTable();
$("#submit").click(function () {
                    var cname = $("#cat_name").val();
					var cid = $("#catid").val();
					var action ="" ;
					if(cid !="")
					{
						action = "update";
					}else{
						action = "add";
					}
                   // $("#errorblock").hide();
                    //$("#successblock").hide();
                    if (cname == "") {
                        $("#error_text").html("Please Fill Category Name.");
                        $("#errorblock").show();
                        return false;
                    }else {
						
                        addCat(cname,cid,action);
                    }
                });
});
</script>
  </body>
</html>