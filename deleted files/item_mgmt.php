<?php 
include("inc/connect.php");
include("inc/userAuth.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Food Item Management - Ndspl Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">Ndspl</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i></a>
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">You have 4 new notifications.</li>
            <div class="app-notification__content">
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Lisa sent you a mail</p>
                    <p class="app-notification__meta">2 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Mail server not working</p>
                    <p class="app-notification__meta">5 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Transaction complete</p>
                    <p class="app-notification__meta">2 days ago</p>
                  </div></a></li>
              <div class="app-notification__content">
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Lisa sent you a mail</p>
                      <p class="app-notification__meta">2 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Mail server not working</p>
                      <p class="app-notification__meta">5 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Transaction complete</p>
                      <p class="app-notification__meta">2 days ago</p>
                    </div></a></li>
              </div>
            </div>
            <li class="app-notification__footer"><a href="#">See all notifications.</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <?php
	include("inc/menu.php");
	?>
      
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> Food Item Manegment</h1>
          <p>Item List</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Food Item Manegment</li>
          <li class="breadcrumb-item"><a href="add_food_item.php">Add Item</a></li>
        </ul>
      </div>
    
	<div class="row">
        <div class="col-md-12">
            <div class="tile">
				<div class="table-responsive">
				  <table class="table table-hover table-bordered" id="catTable">
					<thead>
					  <tr>
						<th>S.No.</th>
						<th>Name</th>
						<th>Category</th>
						<th>Price</th>
						<th>Description</th>
						<th>Status</th>
						<th>Edit</th>
						<th>Delete</th>
					  </tr>
					</thead>
						<tbody>
						<?php
							
								$sql = "select f.*,c.cat_name from food_items f 
								Left JOIN category c on c.cat_id=f.cat_id 
								order by f.item_id desc";
								$rs = mysqli_query($con,$sql);
								$i=1;
								while($row = mysqli_fetch_array($rs))
								{   $itemname=$row['item_name'];
							        
									
									if($row['item_status']==0) $st="Inactive";
									if($row['item_status']==1) $st="Active";
									echo "<tr><td>".$i."</td>";
									echo "<td>".$itemname."</td>";
									echo "<td>".$row['cat_name']."</td>";
									echo "<td>".$row['item_price']."</td>";
									echo "<td>".$row['item_details']."</td>";
									echo "<td>".$st."</td>";
									echo "<td><a href='edit_food_item.php?id=".$row['item_id']."'>Edit</a></td>";
                                    echo "<td><a href='del_food_item.php?id=".$row['item_id']."'>Delete</a></td>" ;
									echo "</tr>";
									$i=$i+1;
									
								}
							
							
							
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
	<script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
	<script>
	
	 function addCat(cname,cid,act) {
		 //alert(cname);
                $.ajax({type: "POST",
                    url: "add_cat.php",
                    data: "cname=" + cname + "&action=" + act + "&cid=" +cid,
                    async: false,
                    success: function (response) {
						//alert(response);
                        var response_1 = response.charAt();
                        if (response_1 == "f") {
                            $("#error_text").html("Please try again");
                            $("#errorblock").show();
                            setTimeout('$("#errorblock").fadeOut("slow");', 2000);
                            return false;
                        }
						else if (response_1 == "d") {
                            $("#error_text").html("This category alreday Exists");
                            $("#errorblock").show();
                            setTimeout('$("#errorblock").fadeOut("slow");', 2000);
                            return false;
                        }
						else if (response_1 == "s") {
							//alert("success");
                            $("#errorblock").hide();
                            $("#category_name").val('');
                            $("#success_text").html("Saved successfully");
                            $("#successblock").show();
                            setTimeout('$("#successblock").fadeOut("slow");', 2000);
							
							setTimeout('window.location.reload();', 1000);
                            if(type !=""){
                                setTimeout('window.location.reload();', 2000);
                            }
                         }
                    }
                });
            }
			
			
			
			function delCat(cid) {
		     var ans=confirm("Are you sure, you want to delete this category?");
			 if(ans==true){
                $.ajax({type: "POST",
                    url: "add_cat.php",
                    data: "catID=" + cid + "&action=delete" ,
                    async: false,
                    success: function (response) {
						//alert(response);
                        var response_1 = response.charAt();
                        if (response_1 == "f") {
                            $("#error_text").html("Please try again");
                            $("#errorblock").show();
                            setTimeout('$("#errorblock").fadeOut("slow");', 2000);
                            return false;
                        }
						else if (response_1 == "s") {
							//alert("success");
                            $("#errorblock").hide();
                            $("#category_name").val('');
                            $("#success_text").html("Category deleted successfully");
                            $("#successblock").show();
                            setTimeout('$("#successblock").fadeOut("slow");', 2000);
							
							setTimeout('window.location.reload();', 1000);
                            if(type !=""){
                                setTimeout('window.location.reload();', 2000);
                            }
                         }
                    }
                });
            }
			}
			
			
			function editCat(cid) {
			   var str1=cid;
			   var arrCat = str1.split("-");
			   $("#catid").val(arrCat[0]);
			   $("#cat_name").val(arrCat[1]);
				}
			
	
	
$(document).ready( function () {  
$("#errorblock").hide();
$("#successblock").hide();
$('#catTable').DataTable();
$("#submit").click(function () {
                    var cname = $("#cat_name").val();
					var cid = $("#catid").val();
					var action ="" ;
					if(cid !="")
					{
						action = "update";
					}else{
						action = "add";
					}
                   // $("#errorblock").hide();
                    //$("#successblock").hide();
                    if (cname == "") {
                        $("#error_text").html("Please Fill Category Name.");
                        $("#errorblock").show();
                        return false;
                    }else {
						
                        addCat(cname,cid,action);
                    }
                });
});
</script>
  </body>
</html>